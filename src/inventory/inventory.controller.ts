import { Controller, Get, Post, Body, Param, Delete, Put } from '@nestjs/common';
import { ClientGrpc, RpcException } from '@nestjs/microservices';
import { Observable, catchError, throwError } from 'rxjs';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { UpdateInventoryDto } from './dto/update-inventory.dto';
import * as grpc from '@grpc/grpc-js';


@Controller('inventory')
export class InventoryController {
  private inventoryService: any; // Reemplaza con el tipo adecuado del servicio gRPC

  constructor(private client: ClientGrpc) {}

  onModuleInit() {
    this.inventoryService = this.client.getService<any>('InventoryService'); // Reemplaza con el nombre del servicio gRPC
  }

  @Post()
  create(@Body() createInventoryDto: CreateInventoryDto): Observable<any> {
    return this.inventoryService.create(createInventoryDto);
  }

  @Get()
  findAll(): Observable<any[]> {
    return this.inventoryService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Observable<any> {
    return this.inventoryService.findOne({ id }).pipe(
      catchError(err => {
        if (err.code === grpc.status.NOT_FOUND) {
          return throwError(new RpcException({ message: 'Inventory item not found' }));
        }
        return throwError(err);
      }),
    );
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateInventoryDto: UpdateInventoryDto): Observable<any> {
    return this.inventoryService.update({ id, ...updateInventoryDto });
  }

  @Delete(':id')
  delete(@Param('id') id: string): Observable<any> {
    return this.inventoryService.delete({ id }).pipe(
      catchError(err => {
        if (err.code === grpc.status.NOT_FOUND) {
          return throwError(new RpcException({ message: 'Inventory item not found' }));
        }
        return throwError(err);
      }),
    );
  }
}
