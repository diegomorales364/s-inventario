import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { InventoryItem } from './schemas/inventory.schema';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { UpdateInventoryDto } from './dto/update-inventory.dto';

@Injectable()
export class InventoryService {
  constructor(@InjectModel('InventoryItem') private inventoryModel: Model<InventoryItem>) {}

  // Crear un nuevo item de inventario
  async create(createInventoryDto: CreateInventoryDto): Promise<InventoryItem> {
    return this.inventoryModel.create(createInventoryDto);
  }

  // Leer todos los items de inventario
  async findAll(): Promise<InventoryItem[]> {
    return this.inventoryModel.find().exec();
  }

  // Leer un item de inventario por ID
  async findOne(id: string): Promise<InventoryItem> {
    return this.inventoryModel.findById(id).exec();
  }

  // Actualizar un item de inventario
  async update(id: string, updateInventoryDto: UpdateInventoryDto): Promise<InventoryItem> {
    return this.inventoryModel.findByIdAndUpdate(id, updateInventoryDto, { new: true }).exec();
  }

  // Eliminar un item de inventario
  async delete(id: string): Promise<InventoryItem> {
    return this.inventoryModel.findOneAndDelete({ _id: id }).exec();
  }
}
