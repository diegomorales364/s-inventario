import { Test, TestingModule } from '@nestjs/testing';
import { InventoryService } from './inventory.service';
import { getModelToken } from '@nestjs/mongoose';
import { InventoryItem } from './schemas/inventory.schema';

describe('InventoryService', () => {
  let service: InventoryService;

  // Un mock para el modelo de Mongoose
  const mockInventoryModel = {
    create: jest.fn((dto) => Promise.resolve({ _id: 'a uuid', ...dto })),
    find: jest.fn(() => ({
      exec: jest.fn().mockResolvedValue([]),
    })),
    findById: jest.fn((id) => ({
      exec: jest.fn().mockResolvedValue({ _id: id, productId: '123', locationId: '321', quantity: 10 }),
    })),
    findByIdAndUpdate: jest.fn((id, dto) => ({
      exec: jest.fn().mockResolvedValue({ _id: id, ...dto }),
    })),
    findOneAndDelete: jest.fn((condition) => ({
      exec: jest.fn().mockResolvedValue({ _id: condition._id }),
    })),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InventoryService,
        {
          provide: getModelToken('InventoryItem'),
          useValue: mockInventoryModel,
        },
      ],
    }).compile();

    service = module.get<InventoryService>(InventoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new inventory item', async () => {
    const newInventoryDto = { productId: '123', locationId: '321', quantity: 10 };
    expect(await service.create(newInventoryDto)).toEqual({ _id: expect.any(String), ...newInventoryDto });
    expect(mockInventoryModel.create).toHaveBeenCalledWith(newInventoryDto);
  });

  it('should find all inventory items', async () => {
    await service.findAll();
    expect(mockInventoryModel.find).toHaveBeenCalled();
  });

  it('should find one inventory item by id', async () => {
    const id = 'some id';
    await service.findOne(id);
    expect(mockInventoryModel.findById).toHaveBeenCalledWith(id);
  });

  it('should update an inventory item', async () => {
    const id = 'some id';
    const updateInventoryDto = { productId: '123', locationId: '321', quantity: 5 };
    await service.update(id, updateInventoryDto);
    expect(mockInventoryModel.findByIdAndUpdate).toHaveBeenCalledWith(id, updateInventoryDto, { new: true });
  });

  it('should delete an inventory item', async () => {
    const id = 'some id';
    await service.delete(id);
    expect(mockInventoryModel.findOneAndDelete).toHaveBeenCalledWith({ _id: id });
  });
});