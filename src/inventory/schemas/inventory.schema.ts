import * as mongoose from 'mongoose';

export const InventorySchema = new mongoose.Schema({
  productId: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
  locationId: { type: mongoose.Schema.Types.ObjectId, ref: 'Location' },
  quantity: { type: Number, required: true },
});

// Clase que representa el documento
export interface InventoryItem extends mongoose.Document {
  productId: mongoose.Schema.Types.ObjectId;
  locationId: mongoose.Schema.Types.ObjectId;
  quantity: number;
}
