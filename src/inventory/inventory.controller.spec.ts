import { Test, TestingModule } from '@nestjs/testing';
import { InventoryController } from './inventory.controller';
import { InventoryService } from './inventory.service';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { UpdateInventoryDto } from './dto/update-inventory.dto';

describe('InventoryController', () => {
  let controller: InventoryController;
  let service: InventoryService;

  beforeEach(async () => {
    // Crear un mock del InventoryService
    const mockInventoryService = {
      create: jest.fn((dto) => Promise.resolve({ _id: 'a uuid', ...dto })),
      findAll: jest.fn(() => Promise.resolve([{ _id: 'a uuid', productId: '123', locationId: '321', quantity: 10 }])),
      findOne: jest.fn((id) => Promise.resolve({ _id: id, productId: '123', locationId: '321', quantity: 10 })),
      update: jest.fn((id, dto) => Promise.resolve({ _id: id, ...dto })),
      delete: jest.fn((id) => Promise.resolve({ _id: id })),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [InventoryController],
      providers: [
        {
          provide: InventoryService,
          useValue: mockInventoryService,
        },
      ],
    }).compile();

    controller = module.get<InventoryController>(InventoryController);
    service = module.get<InventoryService>(InventoryService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a new inventory item', async () => {
    const newInventoryDto: CreateInventoryDto = { productId: '123', locationId: '321', quantity: 10 };
    expect(await controller.create(newInventoryDto)).toEqual({ _id: expect.any(String), ...newInventoryDto });
    expect(service.create).toHaveBeenCalledWith(newInventoryDto);
  });

  it('should find all inventory items', async () => {
    expect(await controller.findAll()).toEqual([{ _id: 'a uuid', productId: '123', locationId: '321', quantity: 10 }]);
    expect(service.findAll).toHaveBeenCalled();
  });

  it('should find one inventory item by id', async () => {
    const id = 'a uuid';
    expect(await controller.findOne(id)).toEqual({ _id: id, productId: '123', locationId: '321', quantity: 10 });
    expect(service.findOne).toHaveBeenCalledWith(id);
  });

  it('should update an inventory item', async () => {
    const id = 'a uuid';
    const updateInventoryDto: UpdateInventoryDto = { productId: '123', locationId: '321', quantity: 5 };
    expect(await controller.update(id, updateInventoryDto)).toEqual({ _id: id, ...updateInventoryDto });
    expect(service.update).toHaveBeenCalledWith(id, updateInventoryDto);
  });

  it('should delete an inventory item', async () => {
    const id = 'a uuid';
    await controller.delete(id);
    expect(service.delete).toHaveBeenCalledWith(id);
  });
});