import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { InventorySchema } from './schemas/inventory.schema';
import { InventoryService } from './inventory.service';
import { InventoryController } from './inventory.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'InventoryItem', schema: InventorySchema }]),
    // Configuración de gRPC
    ClientsModule.register([
      {
        name: 'INVENTORY_SERVICE',
        transport: Transport.GRPC,
        options: {
          url: 'localhost:50051', // Usar la dirección y puerto correctos del microservicio de inventario
          package: 'inventory', // El nombre del paquete en el archivo .proto
          protoPath: join(__dirname, '../../proto/inventory.proto'), // La ruta a el archivo .proto
        },
      },
    ]),
  ],
  providers: [InventoryService],
  controllers: [InventoryController],
})
export class InventoryModule {}