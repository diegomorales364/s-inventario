import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class CreateInventoryDto {
  @IsString()
  @IsNotEmpty()
  readonly productId: string;

  @IsString()
  @IsNotEmpty()
  readonly locationId: string;

  @IsNumber()
  readonly quantity: number;
}
