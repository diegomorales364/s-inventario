export class UpdateInventoryDto {
    readonly productId?: string;
    readonly locationId?: string;
    readonly quantity?: number;
  }
  